# syntax=docker/dockerfile:1

FROM golang:1.16-alpine

# Add maintainer information 
LABEL maintainer="Similoluwa Okunowo <rexsimiloluwa@gmail.com>"

RUN mkdir /app

# Set current working directory inside the container
WORKDIR /app

# Copy the Go modules dependency file
COPY go.mod ./
COPY go.sum ./

# Download dependencies
RUN go mod download

# Recursively copy all the files from the host machine to the docker container 
COPY ./ ./

# Set environment variables 
ENV PORT 5000 

RUN go get github.com/githubnemo/CompileDaemon
EXPOSE ${PORT}

ENTRYPOINT CompileDaemon --build="go build -mod=readonly server.go" --command=./server
