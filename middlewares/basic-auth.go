package middlewares

import "github.com/gin-gonic/gin"

func BasicAuth() gin.HandlerFunc {
	// using gin's default basic auth middleware
	// pass in the authorized user accounts
	return gin.BasicAuth(gin.Accounts{
		"theblackdove": "adetoyosi",
	})
}
