start-server:
	go run server.go 
test:
	go test ./...
run-server-dev:
	nodemon --exec go run server.go --signal SIGTERM
build-docker:
	sudo docker build -t go-gin-primer . 
run-docker: 
	sudo docker run -p 5000:5000 go-gin-primer
push-docker-hub:
	sudo docker tag go-gin-primer:latest similoluwaokunowo/go-gin-elastic-beanstalk:latest
	sudo docker push similoluwaokunowo/go-gin-elastic-beanstalk:latest
run-heroku-local: 
	heroku local