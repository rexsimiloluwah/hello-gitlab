package main

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/controllers"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/services"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/middlewares"
)

func setupLogOutput() {
	// initialize a log file
	f, _ := os.Create("gin.log")
	// set the default writer
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout) // Write to the file and the OS terminal
}

func main() {
	setupLogOutput()
	//r := gin.Default()
	server := gin.New()

	server.Static("/css", "./templates/css")

	server.LoadHTMLGlob("./templates/*.html")

	// assigning the middlewares
	server.Use(gin.Recovery(), middlewares.Logger())

	// declare controller and services
	videoService := services.NewVideoService()
	videoController := controllers.NewVideoController(videoService)
	userService := services.NewUserService()
	userController := controllers.NewUserController(userService)

	// grouping the api endpoints
	apiRoutes := server.Group("/api/v1")
	{
		apiRoutes.GET("/hello", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"status":  true,
				"message": "Hello World!",
			})
		})

		apiRoutes.GET("/videos", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"status":  true,
				"message": "Successfully fetched videos",
				"data":    videoController.FindAll(),
			})
		})

		apiRoutes.GET("/videos/:id", func(c *gin.Context) {
			video, err := videoController.FindById(c)
			if err == nil {
				c.JSON(http.StatusOK, gin.H{
					"status":  true,
					"message": "Succesfully fetched video",
					"data":    video,
				})
				return
			}
			if err.Error() == "video not found" {
				c.JSON(http.StatusNotFound, gin.H{
					"status": false,
					"error":  err.Error(),
				})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{
					"status": false,
					"error":  err.Error(),
				})
			}
		})

		apiRoutes.POST("/videos", func(c *gin.Context) {
			video, err := videoController.Save(c)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status": false,
					"error":  err.Error(),
				})
				return
			}
			c.JSON(http.StatusCreated, gin.H{
				"status":  true,
				"message": "Successfully added video",
				"data":    video,
			})
		})

		apiRoutes.GET("/users", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"status":  true,
				"message": "Successfully fetched users",
				"data":    userController.FindAll(),
			})
		})

		apiRoutes.GET("/users/:id", func(c *gin.Context) {
			user, err := userController.FindById(c)
			if err == nil {
				c.JSON(http.StatusOK, gin.H{
					"status":  true,
					"message": "Succesfully fetched user",
					"data":    user,
				})
				return
			}
			if err.Error() == "user not found" {
				c.JSON(http.StatusNotFound, gin.H{
					"status": false,
					"error":  err.Error(),
				})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{
					"status": false,
					"error":  err.Error(),
				})
			}
		})

		apiRoutes.POST("/users", func(c *gin.Context) {
			user, err := userController.Create(c)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"status": false,
					"error":  err.Error(),
				})
				return
			}
			c.JSON(http.StatusCreated, gin.H{
				"status":  true,
				"message": "Successfully added user",
				"data":    user,
			})
		})
	}

	viewRoutes := server.Group("/views")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}
	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "5000"
	}
	server.Run(":" + PORT)
}
