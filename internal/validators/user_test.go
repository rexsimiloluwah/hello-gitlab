package validators

import "testing"

func TestUserValidator(t *testing.T) {
	testCases := []struct {
		password string
		expected bool
	}{
		{"simi", false},
		{"simi12", false},
		{"", false},
		{"Ademola4227", true},
		{"Ademo42", false},
	}

	for _, test := range testCases {
		if output := ValidatePassword(test.password); output != test.expected {
			t.Errorf("password %s is invalid", test.password)
		}
	}
}
