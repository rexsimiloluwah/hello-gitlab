package validators

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

func ValidatePasswordField(field validator.FieldLevel) bool {
	return ValidatePassword(field.Field().String())
}

func ValidatePassword(password string) bool {
	secure := true
	tests := []string{".{8,}", ".*[a-z]", ".*[A-Z]"}
	for _, test := range tests {
		matched, _ := regexp.MatchString(test, password)
		if !matched {
			secure = false
			break
		}
	}
	return secure
}
