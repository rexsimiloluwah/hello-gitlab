package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/models"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/services"
)

type VideoController interface {
	Save(c *gin.Context) (models.Video, error)
	FindAll() []models.Video
	FindById(c *gin.Context) (models.Video, error)
	ShowAll(c *gin.Context)
}

type videoController struct {
	service services.VideoService
}

func NewVideoController(service services.VideoService) VideoController {
	return &videoController{
		service: service,
	}
}

func (v *videoController) Save(c *gin.Context) (models.Video, error) {
	var video models.Video
	// ShouldBindJSON reads the body buffer and serializes it to a struct
	err := c.ShouldBindJSON(&video)
	if err != nil {
		return models.Video{}, err
	}
	return v.service.Save(video), nil
}

func (v *videoController) FindAll() []models.Video {
	return v.service.FindAll()
}

func (v *videoController) FindById(c *gin.Context) (models.Video, error) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return models.Video{}, err
	}
	return v.service.FindById(id)
}

// views rendering
func (v *videoController) ShowAll(c *gin.Context) {
	videos := v.service.FindAll()
	// data will be passed to the template programmatically
	data := gin.H{
		"videos": videos,
	}
	c.HTML(http.StatusOK, "index", data)
}
