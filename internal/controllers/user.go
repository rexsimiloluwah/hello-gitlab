package controllers

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/models"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/services"
	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/validators"
)

type UserController interface {
	Create(c *gin.Context) (models.User, error)
	FindAll() []models.User
	FindById(c *gin.Context) (models.User, error)
}

type userController struct {
	service services.UserService
}

var validateUser *validator.Validate

func NewUserController(service services.UserService) UserController {
	validateUser = validator.New()
	validateUser.RegisterValidation("is-valid-password", validators.ValidatePasswordField)
	return &userController{
		service: service,
	}
}

func (u *userController) Create(c *gin.Context) (models.User, error) {
	var video models.User
	// ShouldBindJSON reads the body buffer and serializes it to a struct
	err := c.ShouldBindJSON(&video)
	if err != nil {
		return models.User{}, err
	}
	err = validateUser.Struct(video)
	if err != nil {
		return models.User{}, err
	}
	return u.service.Create(video), nil
}

func (v *userController) FindAll() []models.User {
	return v.service.FindAll()
}

func (v *userController) FindById(c *gin.Context) (models.User, error) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return models.User{}, err
	}
	return v.service.FindById(id)
}
