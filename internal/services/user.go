package services

import (
	"errors"

	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/models"
)

type UserService interface {
	Create(models.User) models.User
	FindAll() []models.User
	FindById(id int) (models.User, error)
}

type userService struct {
	users []models.User
}

func NewUserService() UserService {
	return &userService{}
}

func (service *userService) Create(newUser models.User) models.User {
	service.users = append(service.users, newUser)
	return newUser
}

func (service *userService) FindAll() []models.User {
	return service.users
}

func (service *userService) FindById(id int) (models.User, error) {
	for _, user := range service.users {
		if user.Id == id {
			return user, nil
		}
	}
	return models.User{}, errors.New("user not found")
}
