package services

import (
	"errors"

	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/models"
)

type VideoService interface {
	Save(models.Video) models.Video
	FindAll() []models.Video
	FindById(id int) (models.Video, error)
}

type videoService struct {
	videos []models.Video
}

func NewVideoService() VideoService {
	return &videoService{}
}

func (service *videoService) Save(newVideo models.Video) models.Video {
	service.videos = append(service.videos, newVideo)
	return newVideo
}

func (service *videoService) FindAll() []models.Video {
	return service.videos
}

func (service *videoService) FindById(id int) (models.Video, error) {
	for _, video := range service.videos {
		if video.Id == id {
			return video, nil
		}
	}
	return models.Video{}, errors.New("video not found")
}
