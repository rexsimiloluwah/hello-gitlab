package services

import (
	"testing"

	"github.com/rexsimiloluwah/DOINGS/go-gin-primer/internal/models"
	"github.com/stretchr/testify/assert"
)

func TestSaveVideo(t *testing.T) {
	service := NewVideoService()
	test_id := 1
	test_title := "Test Title"
	test_description := "Test Description"
	test_creator := "Test Creator"
	test_url := "https://www.youtube.com/embed/BCg4U1FzODs"
	test_created_at := 1647041204552

	newVideo := models.Video{
		Id:          test_id,
		Title:       test_title,
		Description: test_description,
		Creator:     test_creator,
		URL:         test_url,
		CreatedAt:   float64(test_created_at),
	}

	service.Save(newVideo)
	videos := service.FindAll()
	video := videos[0]
	assert.NotNil(t, video)
	assert.Equal(t, video.Id, test_id)
	assert.Equal(t, video.Description, test_description)
	assert.Equal(t, video.Title, test_title)
	assert.Equal(t, video.URL, test_url)
	assert.Equal(t, video.Creator, test_creator)
}
