package models

type Video struct {
	Id          int     `json:"id"`
	Title       string  `json:"title" binding:"min=10,max=200,required"`
	Description string  `json:"description" binding:"max=1000"`
	Creator     string  `json:"author" binding:"required"`
	URL         string  `json:"url" binding:"url"`
	CreatedAt   float64 `json:"created_at"`
}
