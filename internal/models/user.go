package models

type User struct {
	Id        int     `json:"id"`
	Firstname string  `json:"firstname" binding:"required"`
	Lastname  string  `json:"lastname" binding:"required"`
	Email     string  `json:"email" binding:"required,email"`
	Password  string  `json:"password" binding:"required,min=8" validate:"is-valid-password"`
	CreatedAt float64 `json:"created_at"`
}
