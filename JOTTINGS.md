1. Understanding Gin basics and server configuration 

2. Understand middlewares
* middlewares can be used for logging, authorization, and debugging. 

3. Server-side data-binding and validation with the GIN framework

4. Working with HTML and templates using the Gin framework

5. Deployment with AWS Elastic Beanstalk (Elastic beanstalk provides a PaaS offering with features for CI/CD, auto-scaling, load-balancing, health monitoring, capacity provisioning etc)

## Deployment to Elastic beanstalk (EB) in a platform-specific way (i.e. without containers)
1. Initialize the EB configuration 
```bash
$ eb init 
```
2. Create a new environment with a single instance 
```bash
$ eb  create --single 
```

## Deployment to Elastic beanstalk (EB) in a platform-neutral way (i.e. using a docker container)
